from crawl.models import *
from django.contrib import admin

def show_brand(obj):
    try:
        return ("%s" % (obj.brand.all()[0].name))
    except:
        return "None"
show_brand.short_description = 'Brand'

def show_categories(obj):
    try:
        cat = ""
        print obj.category.all()
        for c in obj.category.all():
            cat = cat + ">" + c.name
        return cat
    except:
        return "None"
show_categories.short_description = 'Category'

def show_quantity(obj):
    try:
        return ("%s" % (obj.quantity.all()[0].name))
    except:
        return "None"
show_quantity.short_description = 'Quantity'


class ItemAdmin(admin.ModelAdmin):
    list_display  = ('name', 'price', 'discount', 'image_url', show_brand, show_categories, show_quantity, 'url')

admin.site.register(Item, ItemAdmin)
admin.site.register(Url)
admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Quantity)
