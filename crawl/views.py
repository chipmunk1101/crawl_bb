# Create your views here.
import simplejson as json
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, user_passes_test
from datetime import datetime,timedelta
from crawl.models import *

def brands(request):
    suggestions = []
    for result in Brand.objects.all():
        suggestions.append({'name': result.name})
    the_data = json.dumps({
        'results': suggestions,
        })
    return HttpResponse(the_data, content_type='application/json')

def quants(request):
    suggestions = []
    for result in Quantity.objects.all():
        suggestions.append({'name': result.name})
    the_data = json.dumps({
        'results': suggestions,
        })
    return HttpResponse(the_data, content_type='application/json')

def cats(request):
    suggestions = []
    for result in Category.objects.all():
        suggestions.append({'name': result.name})
    the_data = json.dumps({
        'results': suggestions,
        })
    return HttpResponse(the_data, content_type='application/json')

def items(request):
    suggestions = []
    for result in Item.objects.all():
        try:
            suggestions.append({'name': result.name, 'des': result.description,
                                'p': result.price, 'sp':result.discount, 'quant' : result.quantity.all()[0].name,
                                'cat': result.category.all()[0].name, 'brand': result.brand.all()[0].name})
        except:
            pass
    the_data = json.dumps({
        'results': suggestions,
        })
    return HttpResponse(the_data, content_type='application/json')