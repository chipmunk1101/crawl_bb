import re
import httplib
import urllib2, urllib
from urlparse import urlparse
import BeautifulSoup
from datetime import datetime
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
import time
from httplib2.socks import HTTPError
from crawl.models import *
from django.utils.encoding import smart_unicode
import ast

def converToNumber(val):
    val = val.replace("Rs.", "").strip()
    if "(" in val:
        val = val.split("(")[0]
    val = filter(lambda x:  x.isdigit() or x==".", val)
    val = val.lstrip(".").rstrip(".")
    return val

def lrstrip(v):
    return v.lstrip(" ").rstrip(" ")

def crawlItems(seed):
    tocrawl=[seed]
    crawled=[]
    while tocrawl:
        page=tocrawl.pop()
        print 'Crawled:'+page
        try:
            pagesource=urllib2.urlopen(page)
        except:
            return
        s=pagesource.read()
        soup=BeautifulSoup.BeautifulSoup(s)
        each_div = soup.find("div", { "class" : "fdpSkuArea", "itemscope": "itemscope" })
        f = open('crawled/'+'soup'+'.html','w')
        f.write(str(each_div))
        f.close()

        name_tag = each_div.find("span", { "itemprop" : "name"})
        name = ''.join(name_tag.findAll(text=True))

        price_tag = each_div.find("span", { "itemprop" : "price"})
        price = ''.join(price_tag.findAll(text=True))
        price = price.replace("Rs.", "")
        price = converToNumber(price)

        brand_tag = each_div.find("span", { "itemprop" : "brand"})
        brand = ''.join(brand_tag.findAll(text=True))

        desc_tag = each_div.find("div", { "class" : "fdpSkuDesc"})
        p_desc_tag = desc_tag.findNext("p")
        desc = ''.join(p_desc_tag.findAll(text=True))
        desc = smart_unicode(desc)

        image_tag = each_div.find("img", { "itemprop" : "image"})
        image = image_tag['src']

        img_temp = NamedTemporaryFile()

        i = "http:" + str(image)
        print "Image location :" + str(i)
        try:
            img_temp.write(urllib2.urlopen(i).read())
        except:
            pass
        img_temp.flush()

        discount_tag = each_div.find("div", { "class" : "save"})
        try:
            discount = ''.join(discount_tag.findAll(text=True))
            discount = converToNumber(discount)
        except:
            discount = "0"


        bc = []
        for b in soup.findAll("div", { "class" : "breadcrumb-item" }):
            bc.append(''.join(b.findAll(text=True)))

        quan_tag = each_div.find("span", { "itemprop" : "model"})
        quantity = ''.join(quan_tag.findAll(text=True))
        if ":" in quantity:
            quantity = quantity.split(":")[1]

        brand_obj, is_new = Brand.objects.get_or_create(name=lrstrip(brand))

        quantity_obj, is_new = Quantity.objects.get_or_create(name=lrstrip(quantity))

        item = Item.objects.create(name=lrstrip(name), price=lrstrip(price), discount=lrstrip(discount),
                                   description=lrstrip(desc), image_url=lrstrip(image), image=File(img_temp), brand_raw=brand,
                                       category_raw=str(bc), quantity_raw=quantity)
        for b in bc:
            obj, is_new = Category.objects.get_or_create(name=lrstrip(b))
            item.category.add(obj)

        item.brand.add(brand_obj)
        item.quantity.add(quantity_obj)

        item.save()

    return crawled

def correctItems():
    #for category in Category.objects.all():
    #    if "&" in category.name:
    #        category.name = category.name.split("&")[0]
    #    category.save()

    for item in Item.objects.filter(url='None'):
        print item
        categories = ast.literal_eval(item.category_raw)
        [item.category.remove(x) for x in item.category.all()]
        for category in categories:
            obj, is_new = Category.objects.get_or_create(name=lrstrip(category))
            if 'You are in' in obj.name:
                continue
            item.category.add(obj)

        if 'http:' not in item.image_url:
            item.image_url = 'http:' + item.image_url
        if '/m/' in item.image_url:
            url = item.image_url.split("/m/")[1]
            url = url.split("_")[0]
            item.url = "http://bigbasket.com/pd/" + url

        item.save()

def crawler(SeedUrl):
    tocrawl=[SeedUrl]
    crawled=[]
    while tocrawl:
        page=tocrawl.pop()
        print 'Crawled:'+page
        pagesource=urllib2.urlopen(page)
        s=pagesource.read()
        soup=BeautifulSoup.BeautifulSoup(s)
        f = open('crawled/'+'soup'+'.html','w')
        f.write(str(soup))
        f.close()
        for each_div in soup.findAll("div", { "class" : "skuName" }):
            url, is_created = Url.objects.get_or_create(url=each_div.a['href'])
            print each_div.a['href']

    return crawled


def savePage(i, SeedUrl):
    tocrawl=[SeedUrl]
    crawled=[]
    proxy = urllib2.ProxyHandler({'https': '111.119.217.129:8080'})
    opener = urllib2.build_opener(proxy)
    urllib2.install_opener(opener)
    while tocrawl:
        page=tocrawl.pop()
        print 'Crawled:'+page
        pagesource=urllib2.urlopen(page)
        s=pagesource.read()
        s = str(s).replace('\\"', '')
        s = ''.join(e for e in s if s!='\"')
        f = open('crawled/'+str(i)+'.html','w')
        f.write(s) # python will convert \n to os.linesep
        f.close()

def savePageLoop():
    for i in range(1, 259):
        savePage(i, 'http://bigbasket.com/product/facet/get-page/?page='+str(i))

def getUrlsLoop():
    for i in range(1, 259):
        crawler('file:///E:/crawlBB/crawlBB/crawled/' + str(i) + '.html')

def getItems():
    urls = Url.objects.filter(crawled=False)
    for u in urls:
        crawlItems('http://bigbasket.com'+str(u.url))
        u.crawled = True
        u.save()

def correctQuant():
    for q in Quantity.objects.all():
        if 'Please' in q.name:
            q.name = lrstrip(q.name.split('Please')[0])
            q.save()

def correctCat():
    for c in Category.objects.all():
        if "&" in c.name:
            c.name = c.name.split("&")[0]
        c.save()