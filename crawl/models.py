from django.db import models

# Create your models here.
class Url(models.Model):
    url = models.URLField(unique=True)
    crawled = models.BooleanField(default=False)

    def __unicode__(self):
        return self.url

class Item(models.Model):
    name = models.CharField(max_length=200)
    price = models.FloatField()
    discount = models.FloatField()
    description = models.CharField(max_length=2000)
    image_url = models.CharField(max_length=500)
    brand_raw = models.CharField(max_length=200)
    quantity_raw = models.CharField(max_length=200)
    category_raw = models.CharField(max_length=500)
    image = models.ImageField(upload_to='images')
    category = models.ManyToManyField('Category')
    brand = models.ManyToManyField('Brand')
    quantity = models.ManyToManyField('Quantity')
    url = models.CharField(max_length=500)

    def __unicode__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

class Brand(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

class Quantity(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name
