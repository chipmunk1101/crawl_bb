from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'crawlBB.views.home', name='home'),
    # url(r'^crawlBB/', include('crawlBB.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^get/brands/', 'crawl.views.brands'),
    url(r'^get/items/', 'crawl.views.items'),
    url(r'^get/cat/', 'crawl.views.cats'),
    url(r'^get/quant/', 'crawl.views.quants'),
    #Static file urls
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),

    #Media urls go here
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.FILES_ROOT, 'show_indexes': True}),
)
