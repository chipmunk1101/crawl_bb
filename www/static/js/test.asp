<%
' this is an example server side ASP script to illustrate a simple login routine
' you can use ANY scripting language that you want (asp, php, coldfusion, etc.)

' get the form values posted from the sign in form
frmUN = Trim(Request.Form("username"))
frmPW = Trim(Request.Form("password"))

' this is where you put your login routine (which is beyond the scope of this tutorial)
' you should also include server side validation on the form values to defend against sql injection
' the following simply checks the form values against hard coded values
If frmUN = "johnsmith" AND frmPW = "password" Then
	' if the username and password are verified against your database
	' write 'OK' and set your session 'loggedin' value to True
	Response.Write("OK")
	Session("loggedin") = True
Else
	' if the username and password were not found in your database
	' then return an error message
	Response.Write("<p>Wrong username or password. Please try again.</p>")
End If
%>