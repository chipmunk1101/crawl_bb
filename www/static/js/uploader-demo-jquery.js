$(document).ready(function() {
    var errorHandler = function(event, id, fileName, reason) {
        qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
    };

    var fileNum = 0;
		
	privateVal = 1;
	
	$("input[name='privacy']").change(function() {
		privateVal = $("input[name='privacy']:checked").val();
		alert(privateVal);
	});
	
	
    $('#basicUploadSuccessExample').fineUploader({
		debug: true,
        request: {
            endpoint: "/upload/receiver/",
            paramsInBody: false,
            params: {
				isPrivate: function(){
					$("input[name='privacy']").change(function() {
						privateVal = $("input[name='privacy']:checked").val();
						
					});
					return privateVal;
				},
				fileNum: function() {
                    fileNum+=1;
                    return fileNum;
                }
            }
        },
        chunking: {
            enabled: true
        },
        resume: {
            enabled: true
        },
		validation: {
            sizeLimit: 52428800
        },
        retry: {
            enableAuto: true,
            showButton: true
        }
    })
        .on('error', errorHandler)
        .on('uploadChunk resume', function(event, id, fileName, chunkData) {
            qq.log('on' + event.type + ' -  ID: ' + id + ", FILENAME: " + fileName + ", PARTINDEX: " + chunkData.partIndex + ", STARTBYTE: " + chunkData.startByte + ", ENDBYTE: " + chunkData.endByte + ", PARTCOUNT: " + chunkData.totalParts);
        });

    $('#manualUploadModeExample').fineUploader({
        autoUpload: false,
        uploadButtonText: "Select Files",
        request: {
            endpoint: "/upload/receiver/"
        }
    }).on('error', errorHandler);

    $('#triggerUpload').click(function() {
        $('#manualUploadModeExample').fineUploader("uploadStoredFiles");
    });


    $('#basicUploadFailureExample').fineUploader({
        request: {
            endpoint: "/upload/receiver/",
            params: {"generateError": true}
        },
        failedUploadTextDisplay: {
            mode: 'custom',
            maxChars: 5
        }
    }).on('error', errorHandler);


    $('#uploadWithVariousOptionsExample').fineUploader({
        multiple: false,
        request: {
            endpoint: "/upload/receiver/"
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'txt'],
            sizeLimit: 50000
        },
        text: {
            uploadButton: "Click Or Drop"
        }
    }).on('error', errorHandler);


    $('#fubExample').fineUploader({
        uploaderType: 'basic',
        multiple: false,
        autoUpload: false,
        button: $("#fubUploadButton"),
        request: {
            endpoint: "/upload/receiver/"
        }
    }).on('error', errorHandler);
});
