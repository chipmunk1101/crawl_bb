
mover = -1;
$(function(){
    $('#current_location').keyup(function(e){
        if(e.keyCode == 38){
            mover--;
            if(mover <= 0)
                mover=0;
            $('.auto-data').css('background-color', 'white');
            var ele = $('.auto-data').eq(mover);
            console.log(mover);
            ele.css("background-color", "#F0F0F0");
            return false;
        }
        else if(e.keyCode == 40){
            mover++;
            var totalClassLength = ($('.auto-data').length)-1;
            if(mover >= totalClassLength)
                mover=totalClassLength;
            $('.auto-data').css('background-color', 'white');
            var ele = $('.auto-data').eq(mover);
            console.log(mover);
            ele.css("background-color", "#F0F0F0");
            $('#current_location').val(ele.find("a").attr("actualVal"));
            return false;
        }
        removeAutoTags();
        var search = $('#current_location').val()
        $.ajax({
                url: '/search/zone/auto'
                , data: {
                'q': search
                }
            ,success: function(data) {
                removeAutoTags();
                var autoSource = document.querySelector("#auocomplete-template").innerHTML;
                var autoTemplate = Handlebars.compile(autoSource);
                var results = data.results || [];

                for(var res_offset in results) {
                    var match = results[res_offset].data.match(new RegExp(search, "i"));
                    var hightlighted_string = results[res_offset].data;
                    var autoHtml = autoTemplate({ data : hightlighted_string, match: match });
                    $("#autocomplete-table").append(autoHtml);
                }
            }
        })
    });

    $(document).on('click', '.auto-data',  function(){
        $('#current_location').val($(this).find("a").attr("actualVal"));
        removeAutoTags();
    });

 });



function removeAutoTags(){
    $(".auto-data").remove();
}

Handlebars.registerHelper('hightlight', function(text, match) {
    var result = '<a actualVal="' +  text  + '" class="auto-a">'
        + text.replace(match, "<b>" + match + "</b>") + '</a>';
    return new Handlebars.SafeString(result);
});
