function startProgressBar(){

    $('#main-progress-bar').css({"display": "block" });
    firstProgressBarTime=setInterval(function(){$('#main-progress-bar').find(".first").css({"width" : randomPercentage()+"%"})},80);
    secondProgressBarTime=setInterval(function(){$('#main-progress-bar').find(".second").css({"width" : randomPercentage()+"%"})},80);
    thirdProgressBarTime=setInterval(function(){$('#main-progress-bar').find(".third").css({"width" : randomPercentage()+"%"})},80);
    fourthProgressBarTime=setInterval(function(){$('#main-progress-bar').find(".fourth").css({"width" : randomPercentage()+"%"})},80);
}

function stopProgressBar(){
    $('#main-progress-bar').css({"display": "none" });
    window.clearInterval(firstProgressBarTime)
    window.clearInterval(secondProgressBarTime)
    window.clearInterval(thirdProgressBarTime)
    window.clearInterval(fourthProgressBarTime)


}

function randomPercentage(){
    return Math.random()*100;
}