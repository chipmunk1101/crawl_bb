
mover = -1;
$(function(){

    $('#people').keyup(function(e){
        handleAutocompete(e, $('#people'), $('.people-auto-data'),
            $("#people-auocomplete-template"), $("#people-autocomplete-table"),
            '/search/user/auto');
    });

    $(document).on('click', '.people-auto-data',  function(){
        $('#people').val('');
        addUserTags($(this).find("a").attr("actualVal"))
        removeElement($(this));
    });

    $(document).on('click', '.user-tag',  function(){
        $(this).remove();
    });
});

