function AutoComplete(elem, url){
    this.elem = elem;
    this.url = url;
}

AutoComplete.prototype.InializeEvents = function(){
    var elem = this.elem;
    var url = this.url;
    $('#'+this.elem).keyup(function(e){
        handleAutocompete(e, $('#'+elem), $('.'+ elem +'-auto-data'),
            $('#'+elem+'-autocomplete-template'), $('#'+elem+'-autocomplete-table'),
            url);
    });

    $(document).on('click', '.'+this.elem+'-auto-data',  function(){
        $('#'+elem).val('');
        addTag(elem, $(this).find(".auto-a").attr("actualVal"), $(this).find(".auto-a").attr("idVal"));
        removeElement($('.'+ elem +'-auto-data'));
    });

    $(document).on('click', '.'+this.elem+'-tag',  function(){
        $(this).remove();
    })
}

$(function(){

    createList();
    $(document).on('mouseenter', '.restaurant-data',  function(){
        $(this).css({"background": "#ecf0f1"});
    });
    $(document).on('mouseleave', '.restaurant-data',  function(){
        $(this).css({"background": "white"});
    });

    var subZones = new AutoComplete('spec-loc', '/search/subzone/auto');
    subZones.InializeEvents();
    var people = new AutoComplete('people', '/search/user/auto');
    people.InializeEvents();
    var cuisines = new AutoComplete('cuisines', '/search/cuisine/auto');
    cuisines.InializeEvents();

    $('#get-list').click(function(){
        var subzones = [];
        var users = [];
        var cuisines = [];

        $(".spec-loc-tag").each(function() {
            subzones.push({'subzone': $(this).html()});
        });
        $(".people-tag").each(function() {
            users.push({'user': $(this).attr("idVal")});
        });
        $(".cuisines-tag").each(function() {
            cuisines.push({'cuisine': $(this).html()});
        });

        createList({subzones: subzones, users: users, cuisines: cuisines,
            others: $('#other-keywords').val(), type: $('input[name=restaurant-type]:checked').val(),
            is_budget: $('#budget-list').is(':checked') });
    });
});

function createList(args){
    removeRestaurant();
    startProgressBar();
    var pluralize = function(string) {return string + 's';}
    if (args != undefined){
        $(['subzone', 'user', 'cuisine']).each(function(i, elem) {
            var subzones = args[pluralize(elem)] || []
            var subzones_list = '';
            for(var offset in subzones)
                subzones_list = subzones[offset][elem] + ',' + subzones_list;
            data[pluralize(elem)] = subzones_list;
        });
        console.log(args['is_budget']);
        data['others'] = args['others'];
        data['type'] = args['type'];
        data['is_budget'] = args['is_budget'];
    }
    else{
        data = {}
    }
    $.ajax({
        type: 'GET',
        url: '/restaurant/mainlist'
        , data:data
        ,success: function(data) {
            stopProgressBar();
            var results = data.results || [];
            var source = document.querySelector("#restaurant-template").innerHTML;
            var template = Handlebars.compile(source);
            for(var res_offset in results) {
                var html = template({data:results[res_offset]});
                $("#restaurants").append(html);
            }
        }
    })
}

function handleAutocompete(e, inputElement, dataElement,
                           templateElement, tableElement, URL){
    if(e.keyCode == 38){
        mover--;
        if(mover <= 0)
            mover=0;
        dataElement.css('background-color', 'white');
        var ele = dataElement.eq(mover);
        ele.css("background-color", "#F0F0F0");
        inputElement.val(ele.find(".auto-a").attr("actualVal"));
        return false;
    }
    else if(e.keyCode == 40){
        mover++;
        var totalClassLength = (dataElement.length)-1;
        if(mover >= totalClassLength)
            mover=totalClassLength;
        dataElement.css('background-color', 'white');
        var ele = dataElement.eq(mover);
        ele.css("background-color", "#F0F0F0");
        inputElement.val(ele.find(".auto-a").attr("actualVal"));
        return false;
    }
    else if(e.keyCode == 40){
        removeElement(dataElement);
        return false;
    }
    removeElement(dataElement);
    var search = inputElement.val()
    $.ajax({
        url: URL
        , data: {
            'q': search
        }
        ,success: function(data) {
            removeElement(dataElement);
            mover = -1;
            var autoSource = templateElement.html();
            var autoTemplate = Handlebars.compile(autoSource);
            var results = data.results || [];

            for(var res_offset in results) {
                var match = results[res_offset].data.match(new RegExp(search, "i"));
                var hightlighted_string = results[res_offset].data;
                var autoHtml = autoTemplate({ data : hightlighted_string, match: match, datas:results[res_offset] });
                tableElement.append(autoHtml);
            }
        }
    })
}

function removeElement(e){
    //$(".spec-loc-auto-data").remove();
    e.remove();
}

function addTag(){
    var color_mapping = {'people' : 'success', 'cuisines' : 'warning', 'spec-loc' :'primary'};
    $('.tags').
        append('<button style="margin: 5px;" type="button" class="btn btn-'+
        color_mapping[arguments[0]] +' btn-sm ' + arguments[0] + '-tag" idVal="'+ arguments[2] + '">'
        + arguments[1] + '</button>');
}


function removeRestaurantDetail(){
    $(".restaurant-detail-div").remove();
}

function removeRestaurant(){
    $(".restaurant-data").remove();
}

$(".restaurant-data").click(function(){
    removeRestaurantDetail();
    var source = document.querySelector("#restaurant-detail-template").innerHTML;
    var template = Handlebars.compile(source);
    var autoHtml = template({ data : "asd" });
    $("#restaurant-detail").append(autoHtml);
});

$(document).mouseup(function (e){
    var container_loc = $("#spec-loc-autocomplete-table");

    if (!container_loc.is(e.target) // if the target of the click isn't the container...
        && container_loc.has(e.target).length === 0) // ... nor a descendant of the container
    {
        removeElement($('.spec-loc-auto-data'));
    }

    var container_ppl = $("#people-autocomplete-table");

    if (!container_ppl.is(e.target) // if the target of the click isn't the container...
        && container_ppl.has(e.target).length === 0) // ... nor a descendant of the container
    {
        removeElement($('.people-auto-data'));
    }

    var container_cui = $("#cuisine-autocomplete-table");

    if (!container_cui.is(e.target) // if the target of the click isn't the container...
        && container_cui.has(e.target).length === 0) // ... nor a descendant of the container
    {
        removeElement($('.cuisine-auto-data'));
    }

});

Handlebars.registerHelper('listvertical', function(items, options) {
    var out = "";

    for(var i=0, l=items.length; i<l; i++) {
        out = out + " " + options.fn(items[i]) + " </br></br>";
    }
    return out;
});

Handlebars.registerHelper('listhorizontal', function(items, options) {
    var out = "";

    for(var i=0, l=items.length; i<l; i++) {
        out = out + " " + options.fn(items[i]) + " ";
    }
    return out;
});


Handlebars.registerHelper('hightlight', function() {
    var result = '<a idVal="' +  arguments[2]  + '" actualVal="' +  arguments[0]  + '" class="auto-a">'
        + arguments[0].replace(arguments[1], "<b>" + arguments[1] + "</b>") + '</a>';
    return new Handlebars.SafeString(result);
});